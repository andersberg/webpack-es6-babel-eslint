const path = require('path');


module.exports = {
  paths: {
    root: process.cwd(),
    src: path.resolve(process.cwd(), 'src'),
    packages: path.resolve(process.cwd(), 'packages'),
    output: path.resolve(process.cwd(), 'dist'),
    templates: path.resolve(process.cwd(), 'platform', 'templates'),
  },
};