const path = require('path');
const readPackage = require('read-pkg');
const writePackage = require('read-pkg');

(async () => {
  const packageJson = await readPackage({ cwd: path.resolve(__dirname) });
  console.log('package before', packageJson, '\n');
  await writePackage(
    path.resolve(__dirname),
    { 
      ...packageJson,
      files: [
      ...packageJson.files,
      'webpack.config.js',
    ],
  });
  console.log('package after', readPackage.sync({ cwd: path.resolve(__dirname) }), '\n');
})();