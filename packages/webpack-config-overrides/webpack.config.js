const path = require('path');
const workingDirectory = process.cwd();

module.exports =  {
  mode: 'development',

  devtool: 'source-map',

  output: {
    path: path.resolve(workingDirectory, 'lib'),

    filename: 'bundle.[name].js',

    publicPath: '/assets/',
  },

  module: {
    rules: [],
  },
};
