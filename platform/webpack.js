const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const configFactory = require('./webpack.config');
// const overridesConfig = require('webpack-config-package/webpack.overrides');

console.log('webpack mode:', process.env.NODE_ENV)

const args = process.argv.slice(2);
const isWatch = args.includes('watch');
const isServe = args.includes('serve');

const { paths } = require(path.resolve(process.cwd(), 'project.config.js'));

const config = merge([
  configFactory(paths),
  // overridesConfig,
]);

const compiler = webpack(config);

if (isServe) {
  const WebpackDevServer = require('webpack-dev-server');
  const devServerConfig = require('./devServer.config')
  const server = new WebpackDevServer(
    compiler,
    devServerConfig,
  );

  server.listen(8080, '127.0.0.1', () => {
    console.log('Starting server on http://localhost:8080');
  });

} else if (isWatch) {
  const watching = compiler.watch({
    // Example watchOptions
    aggregateTimeout: 300,
    poll: undefined
  }, (err, stats) => { // Stats Object
    // Print watch/build result here...
    // console.log(stats);
  });
} else {
  compiler.run((err, stats) => { // Stats Object
    // ...
  });
}