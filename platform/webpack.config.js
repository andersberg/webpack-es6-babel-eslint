module.exports = props => 'development' !== process.env.NODE_ENV
  ? require('./webpack.production')(props)
  : require('./webpack.development')(props);