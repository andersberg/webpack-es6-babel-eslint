const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const workingDirectory = process.cwd();

const DEFAULTS = {
  paths: {
    src: path.resolve(workingDirectory, 'src', 'app'),
    output: path.resolve(workingDirectory, 'build')
  }
};

const webpackDevelopmentConfig = (paths = DEFAULTS.paths) => ({
  mode: 'development',

  devtool: 'source-map',

  entry: [
    path.join(paths.src, 'index.js'),
  ],

  output: {
    path: paths.output,

    filename: 'bundle.js',

    publicPath: './',
  },

  module: {
    rules: [
      {
        test: /\.{js|jsx}?$/,
    
        include: [
          paths.src,
        ],

        exclude: [
          path.resolve(workingDirectory, 'node_modules'),
          /node_modules/,
          /bower_components/,
        ],

        use: [{
          loader: 'babel-loader'
        }],
      }
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(paths.templates, 'index.html'),
    })
  ],
});

module.exports = webpackDevelopmentConfig;